import dataclasses
import pathlib
import typing
from ..chat.chat_requester import ChatRequester
from ..chat.chat_request_body import ChatRequestBody


@dataclasses.dataclass(frozen=True)
class ChatFileToFile:
    url: str
    temperature: float = 0.7
    max_tokens: int = 800
    max_request_continue_count: int = 10
    http_proxy: str = ""
    https_proxy: str = ""

    def proxies(self) -> dict:
        """Get proxies.

        Returns:
            dict: Proxies.
        """
        http_proxy = self.http_proxy
        https_proxy = self.https_proxy
        proxies = {}
        if http_proxy != "":
            proxies["http"] = http_proxy
        if https_proxy != "":
            proxies["https"] = https_proxy
        return proxies

    def process_folder(
            self,
            input_folder: pathlib.Path,
            output_folder: pathlib.Path,
    ) -> None:
        """Read input_folder, post request, write response to output_folder.

        Place `system_message.txt` and `user_message.txt` in input_folder.
        Their contents are set to system_message and user_message respectively.

        Response message will be written to `response_message.md`.
        Also, response bodies will be written to `response_body_nn.json`,
        where `nn` is sequential number from "00".

        These files are written to a folder having same name with input_folder.
        This folder is created under output_folder.
        If this folder already exists, the process will be skipped.

        Args:
            input_folder (pathlib.Path): Input folder.
            output_folder (pathlib.Path): Output folder.
        """
        url = self.url
        proxies = self.proxies()
        temperature = self.temperature
        max_tokens = self.max_tokens
        max_request_continue_count = self.max_request_continue_count

        print(f"Started processing folder: {input_folder.name}")
        if not input_folder.is_dir():
            print("Skip! Not a folder.")
            return

        # Create output folder for this folder.
        out_folder = output_folder.joinpath(input_folder.name).resolve()
        if out_folder.exists():
            print("Skip! ")
            return
        out_folder.mkdir(exist_ok=True)

        # Read system message and user message from folder.
        system_message_file = input_folder.joinpath("system_message.txt")
        system_message = system_message_file.read_text(encoding="utf_8")

        user_message_file = input_folder.joinpath("user_message.txt")
        user_message = user_message_file.read_text(encoding="utf_8")

        # Create request body.
        req_body = ChatRequestBody(
            max_tokens=max_tokens,
            temperature=temperature
        )
        req_body.add_system_message(system_message)
        req_body.add_user_message(user_message)

        # Start requesting.
        requester = ChatRequester(url=url, proxies=proxies)
        res_body_list = requester.post_recursive(
            body=req_body,
            max_request_count=max_request_continue_count
        )

        # Check if can continue requesting.
        if res_body_list.can_continue():
            print(" ".join([
                "Can continue requesting but stopped.",
                "Reached max_request_continue_count:",
                f"{max_request_continue_count}",
            ]))

        # Write response bodies to files.
        for i in range(len(res_body_list)):
            res_body = res_body_list[i]
            out_body_file_name = f"response_body_{str(i).rjust(2, '0')}.json"
            out_body_file = out_folder.joinpath(out_body_file_name)
            out_body_file.write_text(res_body.to_json(), encoding="utf_8")

        #  Write assistant message to file.
        out_file = out_folder.joinpath("response_message.md")
        out_file.write_text(
            res_body_list.assistant_message(),
            encoding="utf_8"
        )

    def process_folders(
            self,
            input_folders: typing.List[pathlib.Path],
            output_folder: pathlib.Path,
    ) -> None:
        """Read input_folders, post request, write response to output_folder.

        Args:
            input_folders (typing.List[pathlib.Path]): Input folders.
            output_folder (pathlib.Path): Output folder.
        """
        for input_folder in input_folders:
            self.process_folder(input_folder, output_folder)
