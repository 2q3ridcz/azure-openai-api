"""Skeleton for a listlike object for a specific class.

Inherits list class. See here for inheriting list:
https://realpython.com/inherit-python-list/
"""


class SpecificClassList(list):
    """Listlike object for a specific class."""
    def __init__(self, iterable=None):
        if iterable is None:
            iterable = []
        super().__init__(self.validate_item(item) for item in iterable)

    def __setitem__(self, index, item):
        super().__setitem__(index, self.validate_item(item))

    def insert(self, index, item):
        super().insert(index, self.validate_item(item))

    def append(self, item):
        super().append(self.validate_item(item))

    def extend(self, other):
        if isinstance(other, type(self)):
            super().extend(other)
        else:
            super().extend(self.validate_item(item) for item in other)

    def __add__(self, other):
        return super().__add__(
            [self.validate_item(item) for item in other]
        )

    # Does not support __radd__ because list does not support it.
    # def __radd__(self, other):

    def __iadd__(self, other):
        return super().__iadd__(
            [self.validate_item(item) for item in other]
        )

    def validate_item(self, item):
        """Validates if the item's class is the target_class."""
        target_class = self.target_class()
        if isinstance(item, target_class):
            return item
        raise TypeError(
            f"Expected {target_class.__name__} object, "
            f"but was {type(item).__name__}"
        )

    @staticmethod
    def target_class():
        """Returns the class this list expects to hold.

        This method should be overridden to return the class.
        """
        raise NotImplementedError(
            "This method should be overridden to return the class."
        )
