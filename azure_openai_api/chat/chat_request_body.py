"""Wrapper for request body of Azure OpenAI Chat Completions API.

[Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
(https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
"""
import dataclasses


@dataclasses.dataclass(frozen=False)
class ChatRequestBody:
    """Wrapper for request body of Azure OpenAI Chat Completions API.

    [Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
    (https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
    """
    messages: list = dataclasses.field(default_factory=list)
    max_tokens: int = 800
    temperature: float = 0.7

    def copy(self) -> "ChatRequestBody":
        """Create a copy of the object.

        Returns:
            ChatRequestBody: A copy of the object.
        """
        return ChatRequestBody(
            messages=self.messages.copy(),
            max_tokens=self.max_tokens,
            temperature=self.temperature,
        )

    def to_dict(self) -> dict:
        """Convert the object to a dictionary.

        Returns:
            dict: The dictionary representation of the object.
        """
        return {
            "messages": self.messages,
            "max_tokens": self.max_tokens,
            "temperature": self.temperature,
            "stop": None,
        }

    def _add_message(self, role: str, content: str) -> None:
        """Add a message to the request body."""
        allowed_role = ["system", "user", "assistant"]
        if role not in allowed_role:
            raise ValueError(f"role must be one of {allowed_role}")
        self.messages.append({"role": role, "content": content})

    def add_system_message(self, content: str) -> None:
        """Add a system message to the request body."""
        self._add_message(role="system", content=content)

    def add_user_message(self, content: str) -> None:
        """Add a user message to the request body."""
        self._add_message(role="user", content=content)

    def add_assistant_message(self, content: str) -> None:
        """Add an assistant message to the request body."""
        self._add_message(role="assistant", content=content)
