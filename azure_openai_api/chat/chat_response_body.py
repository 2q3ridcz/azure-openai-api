"""Wrapper for response body of Azure OpenAI Chat Completions API.

[Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
(https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
"""
import dataclasses
import json


@dataclasses.dataclass(frozen=False)
class ChatResponseBody:
    """Wrapper for response body of Azure OpenAI Chat Completions API.

    [Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
    (https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
    """
    data: dict

    def to_dict(self):
        """Get the response body as a dictionary.

        Returns:
            dict: The response body as a dictionary.
        """
        return self.data

    def to_json(self):
        """Get the response body as a JSON string.

        Returns:
            str: The response body as a JSON string.
        """
        data = self.data
        return json.dumps(data)

    def can_continue(self) -> bool:
        """Check if the conversation can continue.

        Returns:
            bool: True if the conversation can continue, False otherwise.
        """
        data = self.data
        if data["choices"][0]["finish_reason"] == "length":
            return True
        return False

    def assistant_message(self) -> str:
        """Get the assistant's message.

        Returns:
            str: The assistant's message.
        """
        data = self.data
        return data["choices"][0]["message"]["content"]
