"""Collection of ChatResponseBody"""
from ..framework.specific_class_list import SpecificClassList
from .chat_response_body import ChatResponseBody


class ChatResponseBodyList(SpecificClassList):
    """Collection of ChatResponseBody"""
    @staticmethod
    def target_class():
        """Returns ChatResponseBody class"""
        return ChatResponseBody

    def can_continue(self) -> bool:
        """Returns True if last message can continue"""
        data = self.copy()
        if len(data) == 0:
            return False
        return data[-1].can_continue()

    def assistant_message(self) -> str:
        """Returns the assistant's message"""
        data = self.copy()
        return "".join([x.assistant_message() for x in data])
