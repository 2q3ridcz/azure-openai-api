"""Requester of Azure OpenAI Chat Completions API.

[Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
(https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
"""
import dataclasses
import os
import requests
from .chat_request_body import ChatRequestBody
from .chat_response_body import ChatResponseBody
from .chat_response_body_list import ChatResponseBodyList


@dataclasses.dataclass(frozen=True)
class ChatRequester:
    """Requester of Azure OpenAI Chat Completions API.

    Uses environment variable AZURE_OPENAI_KEY to authenticate.

    [Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
    (https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
    """
    url: str
    proxies: dict

    def post(
            self,
            body: ChatRequestBody,
            timeout: int = 10,
    ) -> ChatResponseBody:
        """Send a POST request to the API.

        Args:
            body (ChatRequestBody): Request body.

        Returns:
            ChatResponseBody: Response body.

        raises:
            ValueError: If AZURE_OPENAI_KEY environment variable is not set.
        """
        url = self.url
        proxies = self.proxies

        api_key = os.getenv("AZURE_OPENAI_KEY")
        if api_key is None:
            raise ValueError(
                "AZURE_OPENAI_KEY environment variable is not set."
            )

        headers = {
            "Content-Type": "application/json",
            "api-key": api_key,
        }
        data = body.to_dict()
        res = requests.post(
            url=url,
            headers=headers,
            data=data,
            proxies=proxies,
            timeout=timeout
        )
        res.raise_for_status()
        return ChatResponseBody(data=res.json())

    def post_recursive(
            self,
            body: ChatRequestBody,
            max_request_count: int = 10
    ) -> ChatResponseBodyList:
        """Send a POST request to the API recursively.

        Send a request, get a response, and until it reaches max_request_count,
        send a request recursively while response indicates continuable.
        Refer to ChatResponseBody.can_continue for the continue condition.

        On recursive request, "continue" is set to the user message.
        If the response cannot continue, return the response list.

        Args:
            body (ChatRequestBody): Request body.
            max_request_count (int, optional):
                Max request count. Defaults to 10.

        Returns:
            ChatResponseBodyList: Response body list.
        """
        req_body = body.copy()
        body_list = ChatResponseBodyList()
        for _ in range(max_request_count):
            res_body = self.post(body=req_body)
            body_list.append(res_body)
            if not res_body.can_continue():
                break

            req_body.add_assistant_message(
                content=res_body.assistant_message()
            )
            req_body.add_user_message(content="continue")
        return body_list
