# pylint: disable=wrong-import-position
import os
import pathlib
import sys

SELF_FILE = pathlib.Path(__file__).resolve()
SELF_FOLDER = SELF_FILE.parent.resolve()
PKG_FOLDER = SELF_FOLDER.parent.resolve()
if str(PKG_FOLDER) not in sys.path:
    sys.path.append(str(PKG_FOLDER))

from azure_openai_api.application.chat_file_to_file import ChatFileToFile


INPUT_FOLDER = SELF_FOLDER.joinpath("input")
OUTPUT_FOLDER = SELF_FOLDER.joinpath("output")
OUTPUT_FOLDER.mkdir(exist_ok=True)

# Set user settings

# set your api key
API_KEY = ""

# set your url
URL = "https://{your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}/chat/completions?api-version={api-version}"

# set your http proxy
HTTP_PROXY = ""

# set your https proxy
HTTPS_PROXY = ""

TEMPERATURE = 0
MAX_TOKENS = 16000
MAX_REQUEST_CONTINUE_COUNT = 10


# Set api_key for use in ChatRequester.
os.environ['AZURE_OPENAI_KEY'] = str(API_KEY)

# Get input folders.
input_folders = list(INPUT_FOLDER.glob("*"))

# Start processing.
app = ChatFileToFile(
    url=URL,
    temperature=TEMPERATURE,
    max_tokens=MAX_TOKENS,
    max_request_continue_count=MAX_REQUEST_CONTINUE_COUNT,
    http_proxy=HTTP_PROXY,
    https_proxy=HTTPS_PROXY,
)
app.process_folders(input_folders, OUTPUT_FOLDER)
