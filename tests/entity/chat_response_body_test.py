import pytest
from azure_openai_api.chat.chat_response_body import ChatResponseBody


@pytest.fixture(scope='function', autouse=True)
def sample_res():
    """Sample response from Microsoft Learn.

    [Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
    (https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
    """
    yield {
        "id":"chatcmpl-6v7mkQj980V1yBec6ETrKPRqFjNw9",
        "object":"chat.completion",
        "created":1679072642,
        "model":"gpt-35-turbo",
        "usage":{
            "prompt_tokens":58,
            "completion_tokens":68,
            "total_tokens":126
        },
        "choices":[{
            "message":{
                "role":"assistant",
                "content":"Yes, other Azure AI services also support customer managed keys. Azure AI services offer multiple options for customers to manage keys, such as using Azure Key Vault, customer-managed keys in Azure Key Vault or customer-managed keys through Azure Storage service. This helps customers ensure that their data is secure and access to their services is controlled."
            },
            "finish_reason":"stop",
            "index":0
        }]
    }


class TestInit:
    def test_error_on_no_data(self, ):
        '''dataなしは許容しない'''
        # Given # When # Then
        with pytest.raises(TypeError) as e:
            ChatResponseBody()


class TestToDict:
    def test_empty_dict(self):
        '''空dictを渡すと空dictを返す'''
        # Given
        in_data = {}
        body = ChatResponseBody(data=in_data)

        # When
        data = body.to_dict()

        # Then
        assert data == in_data

    def test_some_dict(self, sample_res):
        '''dictを渡すとdictを返す'''
        # Given
        in_data = sample_res
        body = ChatResponseBody(data=in_data)

        # When
        data = body.to_dict()

        # Then
        assert data == in_data


class TestCanContinue:
    def test_error_on_not_enough_data(self):
        '''dataに十分な情報がない場合はエラー'''
        # Given
        in_data = {}
        body = ChatResponseBody(data=in_data)

        # When # Then
        with pytest.raises(KeyError) as e:
            body.can_continue()

    def test_false_when_stop(self, sample_res):
        '''finish_reasonがstopの場合はFalseを返す'''
        # Given
        in_data = sample_res
        body = ChatResponseBody(data=in_data)

        # When
        acctual = body.can_continue()

        # Then
        assert acctual == False

    def test_true_when_length(self):
        '''finish_reasonがlengthの場合はTrueを返す'''
        # Given
        in_data = {
            "choices":[{
                "finish_reason":"length",
            }]
        }
        body = ChatResponseBody(data=in_data)

        # When
        acctual = body.can_continue()

        # Then
        assert acctual == True


class TestAssistantMessage:
    def test_error_on_not_enough_data(self):
        '''dataに十分な情報がない場合はエラー'''
        # Given
        in_data = {}
        body = ChatResponseBody(data=in_data)

        # When # Then
        with pytest.raises(KeyError) as e:
            body.assistant_message()

    def test_return_assistant_message(self, sample_res):
        '''Assistant messageを返す'''
        # Given
        in_data = sample_res
        body = ChatResponseBody(data=in_data)

        # When
        acctual = body.assistant_message()

        # Then
        expect = "Yes, other Azure AI services also support customer managed keys. Azure AI services offer multiple options for customers to manage keys, such as using Azure Key Vault, customer-managed keys in Azure Key Vault or customer-managed keys through Azure Storage service. This helps customers ensure that their data is secure and access to their services is controlled."
        assert acctual == expect
