import pytest
from azure_openai_api.chat.chat_request_body import ChatRequestBody


@pytest.fixture(scope='function', autouse=True)
def default_dict():
    '''初期値応答'''
    yield {
        "messages": [],
        "max_tokens": 800,
        "temperature": 0.7,
        "stop": None
    }


class TestToDict:
    def test_default(self, default_dict):
        '''初期値を返す'''
        # Given
        body = ChatRequestBody()

        # When
        data = body.to_dict()

        # Then
        assert data == default_dict


class TestAddSystemMessage:
    def test_add(self, default_dict):
        '''System messageを登録するとsystem messageを応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"

        # When
        body.add_system_message(message)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "system", "content": message,})
        assert body.to_dict() == expected

    def test_add_twice(self, default_dict):
        '''複数登録すると複数を応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"
        message2 = "Hello2"

        # When
        body.add_system_message(message)
        body.add_system_message(message2)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "system", "content": message,})
        expected["messages"].append({"role": "system", "content": message2,})
        assert body.to_dict() == expected


class TestAddUserMessage:
    def test_add(self, default_dict):
        '''User messageを登録するとuser messageを応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"

        # When
        body.add_user_message(message)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "user", "content": message,})
        assert body.to_dict() == expected

    def test_add_twice(self, default_dict):
        '''複数登録すると複数を応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"
        message2 = "Hello2"

        # When
        body.add_user_message(message)
        body.add_user_message(message2)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "user", "content": message,})
        expected["messages"].append({"role": "user", "content": message2,})
        assert body.to_dict() == expected


class TestAddAssistantMessage:
    def test_add(self, default_dict):
        '''System messageを登録するとSystem messageを応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"

        # When
        body.add_assistant_message(message)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "assistant", "content": message,})
        assert body.to_dict() == expected

    def test_add_twice(self, default_dict):
        '''複数登録すると複数を応答する'''
        # Given
        body = ChatRequestBody()
        message = "Hello"
        message2 = "Hello2"

        # When
        body.add_assistant_message(message)
        body.add_assistant_message(message2)

        # Then
        expected = default_dict
        expected["messages"].append({"role": "assistant", "content": message,})
        expected["messages"].append({"role": "assistant", "content": message2,})
        assert body.to_dict() == expected


class TestAddMessage:
    def test_invalid_role(self):
        '''System messageを登録するとsystem messageを応答する'''
        # Given
        body = ChatRequestBody()

        # When # Then
        with pytest.raises(ValueError) as e:
            body._add_message(role="role", content="content")
        assert str(e.value) == "role must be one of ['system', 'user', 'assistant']"
