import json
import os
import pathlib
import pytest
from azure_openai_api.application.chat_file_to_file import ChatFileToFile
from unittest.mock import patch, MagicMock


SCRIPTDIR = pathlib.Path(__file__).parent.resolve()
TESTDATADIR = SCRIPTDIR.joinpath("chat_file_to_file/TestProcessFolders")
INPUTDIR = TESTDATADIR.joinpath("input")
EXPECTDIR = TESTDATADIR.joinpath("expect")


@pytest.fixture(scope='function', autouse=True)
def sample_res():
    """Sample response with `finish_reason` changed to `length`.

    Sample from Microsoft Learn, with `finish_reason` changed to `length`.

    [Azure OpenAI Service の REST API リファレンス - Azure OpenAI | Microsoft Learn]
    (https://learn.microsoft.com/ja-jp/azure/ai-services/openai/reference#chat-completions)
    """
    yield {
        "id":"chatcmpl-6v7mkQj980V1yBec6ETrKPRqFjNw9",
        "object":"chat.completion",
        "created":1679072642,
        "model":"gpt-35-turbo",
        "usage":{
            "prompt_tokens":58,
            "completion_tokens":68,
            "total_tokens":126
        },
        "choices":[{
            "message":{
                "role":"assistant",
                "content":"Yes, other Azure AI services also support customer managed keys. Azure AI services offer multiple options for customers to manage keys, such as using Azure Key Vault, customer-managed keys in Azure Key Vault or customer-managed keys through Azure Storage service. This helps customers ensure that their data is secure and access to their services is controlled."
            },
            "finish_reason":"length",
            "index":0
        }]
    }


class TestProcessFolders:
    @patch("azure_openai_api.chat.chat_requester.requests")
    def test_default(self, mock_requests, sample_res, tmp_path):
        # Given
        os.environ['AZURE_OPENAI_KEY'] = str("API_KEY")
        app = ChatFileToFile(url="URL")
        input_folders = list(INPUTDIR.glob("*"))
        output_folder = tmp_path

        res = MagicMock()
        res.raise_for_status.return_value = ""
        res.json.return_value = sample_res

        mock_requests.post.return_value = res

        # When
        app.process_folders(input_folders, output_folder)

        # Then
        for input_folder in input_folders:
            out_folder = output_folder.joinpath(input_folder.name)
            expect_folder = EXPECTDIR.joinpath(input_folder.name)
            
            response_message = out_folder.joinpath("response_message.md").read_text("utf_8")
            expect_message = expect_folder.joinpath("response_message.md").read_text("utf_8")
            assert response_message == expect_message

            for i in range(10):
                response_body = out_folder.joinpath(f"response_body_0{i}.json").read_text("utf_8")
                response_json = json.loads(response_body)
                expect_body = expect_folder.joinpath(f"response_body_0{i}.json").read_text("utf_8")
                expect_json = json.loads(expect_body)
                assert response_json == expect_json
