# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1] - yyyy-mm-dd
### Added
- ChatFileToFile functionality.

[Unreleased]: https://gitlab.com/2q3ridcz/azure-openai-api/-/compare/v0.0.1...main
[0.0.1]: https://gitlab.com/2q3ridcz/azure-openai-api/-/tree/v0.0.1
